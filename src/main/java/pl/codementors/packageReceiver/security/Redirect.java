package pl.codementors.packageReceiver.security;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * Bean for redirecting after login
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@RequestScoped
@Named
public class Redirect implements Serializable {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * redirecting method for logging in
     * @return path depending on user's role
     */
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        try {
            request.login(username, password);
            if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMIN")){
                return "items_admin?faces-redirect=true";
            } else if(FacesContext.getCurrentInstance().getExternalContext().isUserInRole("USER")){
                return "items?faces-redirect=true";
            } else {
                return "401?faces-redirect=true";
            }
        } catch (ServletException e) {
            e.printStackTrace();
        }
        return "401?faces-redirect=true";
    }

    /**
     * redirecting method for logging out
     * @return path to login.xhtml
     */
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login?faces-redirect=true";
    }
}
