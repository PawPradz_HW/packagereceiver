package pl.codementors.packageReceiver.security;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.security.Principal;

/**
 * This bean checks user's role for rendering specific JSF elements
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@ViewScoped
@Named
public class RoleView implements Serializable {

    /**
     *
     * @return allows to render elements visible for admin
     */
    public boolean isAdmin() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMIN");
    }

    /**
     *
     * @return allows to render elements visible for basic user
     */
    public boolean isUser() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("USER");
    }

    /**
     *
     * @return allows to render elements visible for not logged guest
     */
    public boolean isGuest() {
        Boolean showLoginField = true;
        Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
        if (principal==null){
            showLoginField = true;
        } else {
            showLoginField = false;
        }
        return showLoginField;
    }

}
