package pl.codementors.packageReceiver.services;

import pl.codementors.packageReceiver.model.Tag;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class for fetching Tag from database
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@Stateless
public class TagsServices {

    private static final Logger log = Logger.getLogger(TagsServices.class.getName());

    /**
     * Just an EntityManager to communicate with the database
     */
    @PersistenceContext
    private EntityManager em;

    /**
     *
     * @param id Id of a single tag to be returned
     * @return returns a single tag from the database
     */
    public Tag getTag(Integer id) {
        return em.find(Tag.class, id);
    }

    /**
     *
     * @return returns all tags from the database
     */
    public List<Tag> getTags() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        query.from(Tag.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @param tag single tag that is going to be added into database
     */
    public void createTag(Tag tag) {
        em.persist(tag);
    }

    /**
     *
     * @param tag single tag that is going to be updated in database
     */
    public void updateTag(Tag tag) {
        em.merge(tag);
    }

    /**
     *
     * @param tag single tag that is going to be removed from database
     */
    public void deleteTag(Tag tag) {
        em.remove(em.merge(tag));
    }

    /**
     * This method checks if Tag is assigned to an Item. If it is not, then it can be
     * removed from database
     * @param tag tag to be checked
     * @return true if tag is not assigned to any of items, false otherwise
     */
    public boolean isTagNotUsed(Tag tag) {
        Query query = em.createQuery("select i from Item i join i.tags tag where tag = :tag");
        query.setParameter("tag", tag);
        return query.getResultList().isEmpty();
    }
}


