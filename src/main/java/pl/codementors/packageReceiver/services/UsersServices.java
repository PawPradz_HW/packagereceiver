package pl.codementors.packageReceiver.services;

import pl.codementors.packageReceiver.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class for fetching User from database
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@Stateless
public class UsersServices {

    private static final Logger log = Logger.getLogger(UsersServices.class.getName());

    /**
     * Just an EntityManager to communicate with the database
     */
    @PersistenceContext
    private EntityManager em;

    /**
     *
     * @param login login of a single user to be returned
     * @return returns a single user from the database
     */
    public User getUser(String login){
        return em.find(User.class, login);
    }

    /**
     *
     * @return returns all users from the database
     */
    public List<User> getUsers() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        query.from(User.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @param user single user that is going to be added into database
     */
    public void createUser(User user) {
        em.persist(user);
    }

    /**
     *
     * @param user single user that is going to be updated in database
     */
    public void updateUser(User user) {
        em.merge(user);
    }

    /**
     *
     * @param user single user that is going to be removed from database
     */
    public void deleteUser(User user) {
        em.remove(em.merge(user));
    }

    public boolean isUserTheLastAdminLeft(User user) {
        if(user.getRole() != User.Role.ADMIN) {
            User.Role lol1 = user.getRole();
            return false;
        } else {
            Query query = em.createQuery("select u from User u where u.role = :role");
            query.setParameter("role", User.Role.ADMIN);
            int lol = query.getResultList().size();
            return lol == 1;
            //return query.getResultList().size() == 1; //If result list has size of 1, then user selected for deletion is the last admin left (returns true)
        }
    }
}
