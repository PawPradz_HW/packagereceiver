package pl.codementors.packageReceiver.services;

import pl.codementors.packageReceiver.model.Category;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class for fetching categories from database
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@Stateless
public class CategoriesServices {

    private static final Logger log = Logger.getLogger(CategoriesServices.class.getName());

    /**
     * Just an EntityManager to communicate with the database
     */
    @PersistenceContext
    private EntityManager em;

    /**
     *
     * @param id Id of a single category to be returned
     * @return returns a single category from the database
     */
    public Category getCategory(Integer id){
        return em.find(Category.class, id);
    }

    /**
     *
     * @return returns all categories from the database
     */
    public List<Category> getCategories() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> query = cb.createQuery(Category.class);
        query.from(Category.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @param category single category that is going to be added into database
     */
    public void createCategory(Category category) {
        em.persist(category);
    }

    /**
     *
     * @param category single category that is going to be updated in database
     */
    public void updateCategory(Category category) {
        em.merge(category);
    }

    /**
     *
     * @param category single category that is going to be removed from database
     */
    public void deleteCategory(Category category) {
        em.remove(em.merge(category));
    }

    /**
     * This method checks if Category is assigned to an Item. If it is not, then it can be
     * removed from database
     * @param cat category to be checked
     * @return true if category is not assigned to any of items, false otherwise
     */
    public boolean isCategoryNotUsed(Category cat) {
        Query query = em.createQuery("select i from Item i join i.category c where c = :cat");
        query.setParameter("cat", cat);
        return query.getResultList().isEmpty();
    }
}
