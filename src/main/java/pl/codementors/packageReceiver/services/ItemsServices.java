package pl.codementors.packageReceiver.services;

import pl.codementors.packageReceiver.Util;
import pl.codementors.packageReceiver.model.Item;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class for fetching Items from database
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@Stateless
public class ItemsServices {

    private static final Logger log = Logger.getLogger(ItemsServices.class.getName());

    /**
     * Just an EntityManager to communicate with the database
     */
    @PersistenceContext
    private EntityManager em;

    /**
     *
     * @param id Id of a single item to be returned
     * @return returns a single item from the database
     */
    public Item getItem(int id){
        return em.find(Item.class, id);
    }

    /**
     *
     * @return returns all items from the database
     */
    public List<Item> getItems() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        query.from(Item.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @param item single item that is going to be added into database
     */
    public void createItem(Item item) {
        em.persist(item);
    }

    /**
     *
     * @param item single item that is going to be updated in database
     */
    public void updateItem(Item item) {
        em.merge(item);
    }

    /**
     *
     * @param item single item that is going to be removed along with its picture
     * and thumbnail
     */
    public void deleteItem(Item item) {
        em.remove(em.merge(item));
        File imgToDelete = new File(Util.getImgFolderPath() + "img" + item.getId());
        File thumbToDelete = new File(Util.getImgFolderPath() + "thumb" + item.getId());
        try {
            Files.deleteIfExists(imgToDelete.toPath());
        } catch (IOException ignored) {}
        try {
            Files.deleteIfExists(thumbToDelete.toPath());
        } catch (IOException ignored) {}
    }
}
