package pl.codementors.packageReceiver.services;

import pl.codementors.packageReceiver.model.Warehouse;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class for fetching Warehouse from database
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@Stateless
public class WarehousesServices {

    private static final Logger log = Logger.getLogger(WarehousesServices.class.getName());

    /**
     * Just an EntityManager to communicate with the database
     */
    @PersistenceContext
    private EntityManager em;

    /**
     *
     * @param id Id of a single warehouse to be returned
     * @return returns a single warehouse from the database
     */
    public Warehouse getWarehouse(Integer id) {
        return em.find(Warehouse.class, id);
    }

    /**
     *
     * @return returns all warehouses from the database
     */
    public List<Warehouse> getWarehouses() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Warehouse> query = cb.createQuery(Warehouse.class);
        query.from(Warehouse.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @param warehouse single warehouse that is going to be added into database
     */
    public void createWarehouse(Warehouse warehouse) {
        em.persist(warehouse);
    }

    /**
     *
     * @param warehouse single warehouse that is going to be updated in database
     */
    public void updateWarehouse(Warehouse warehouse) {
        em.merge(warehouse);
    }

    /**
     *
     * @param warehouse single warehouse that is going to be removed from database
     */
    public void deleteWarehouse(Warehouse warehouse) {
        em.remove(em.merge(warehouse));
    }

    /**
     * This method checks if Warehouse is assigned to an Item. If it is not, then it can be
     * removed from database
     * @param war warehouse to be checked
     * @return true if warehouse is not assigned to any of items, false otherwise
     */
    public boolean isWarehouseNotUsed(Warehouse war) {
        Query query = em.createQuery("select i from Item i join i.warehouse w where w = :war");
        query.setParameter("war", war);
        return query.getResultList().isEmpty();
    }
}