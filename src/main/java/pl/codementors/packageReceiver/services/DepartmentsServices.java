package pl.codementors.packageReceiver.services;

import pl.codementors.packageReceiver.model.Department;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class for fetching departments from database
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@Stateless
public class DepartmentsServices {

    private static final Logger log = Logger.getLogger(DepartmentsServices.class.getName());

    /**
     * Just an EntityManager to communicate with the database
     */
    @PersistenceContext
    private EntityManager em;

    /**
     *
     * @param id Id of a single department to be returned
     * @return returns a single department from the database
     */
    public Department getDepartment(Integer id) {
        return em.find(Department.class, id);
    }

    /**
     *
     * @return returns all departments from the database
     */
    public List<Department> getDepartments() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Department> query = cb.createQuery(Department.class);
        query.from(Department.class);
        return em.createQuery(query).getResultList();
    }

    /**
     *
     * @param department single department that is going to be added into database
     */
    public void createDepartment(Department department) {
        em.persist(department);
    }

    /**
     *
     * @param department single department that is going to be updated in database
     */
    public void updateDepartment(Department department) {
        em.merge(department);
    }

    /**
     *
     * @param department single department that is going to be removed from database
     */
    public void deleteDepartment(Department department) {
        em.remove(em.merge(department));
    }

    /**
     * This method checks if Department is assigned to an Item. If it is not, then it can be
     * removed from database
     * @param dep department to be checked
     * @return true if department is not assigned to any of items, false otherwise
     */
    public boolean isDepartmentNotUsed(Department dep) {
        Query query = em.createQuery("select i from Item i join i.department d where d = :dep");
        query.setParameter("dep", dep);
        return query.getResultList().isEmpty();
    }
}
