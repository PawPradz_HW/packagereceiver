package pl.codementors.packageReceiver.view;

import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.IOUtils;
import pl.codementors.packageReceiver.Util;
import pl.codementors.packageReceiver.model.Item;
import pl.codementors.packageReceiver.services.*;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
@ViewScoped
public class CreateItem implements Serializable {

    private static final Logger log = Logger.getLogger(CreateItem.class.getName());

    @EJB
    private ItemsServices itemsServices;
    @EJB
    private CategoriesServices categoriesServices;
    @EJB
    private DepartmentsServices departmentsServices;
    @EJB
    private WarehousesServices warehousesServices;
    @EJB
    private TagsServices tagsServices;


    private Item newItem;

    private Part imageFile;

    private List<SelectItem> categories;
    private List<SelectItem> departments;
    private List<SelectItem> warehouses;
    private List<SelectItem> tags;

    public Item getNewItem() {
        if(newItem == null) {
            newItem = new Item();
        }
        return newItem;
    }

    public void setNewItem(Item newItem) {
        this.newItem = newItem;
    }

    public Part getImageFile() {
        return imageFile;
    }

    public void setImageFile(Part imageFile) {
        this.imageFile = imageFile;
    }

    public List<SelectItem> getCategories() {
        if (categories == null) {
            categories = new ArrayList<SelectItem>();
            categoriesServices.getCategories().forEach(category -> {
                categories.add(new SelectItem(category,category.getName()));
            });
        }
        return categories;
    }

    public List<SelectItem> getDepartments() {
        if (departments == null) {
            departments = new ArrayList<SelectItem>();
            departmentsServices.getDepartments().forEach(department -> {
                departments.add(new SelectItem(department,department.getName()));
            });
        }
        return departments;
    }

    public List<SelectItem> getWarehouses() {
        if (warehouses == null) {
            warehouses = new ArrayList<SelectItem>();
            warehousesServices.getWarehouses().forEach(warehouse -> {
                warehouses.add(new SelectItem(warehouse,warehouse.getName()));
            });
        }
        return warehouses;
    }

    public List<SelectItem> getTags() {
        if (tags == null) {
            tags = new ArrayList<SelectItem>();
            tagsServices.getTags().forEach(tag -> {
                tags.add(new SelectItem(tag,tag.getName()));
            });
        }
        return tags;
    }

    /**
     * This method uploads image provided by user to servers drive after some modifications.
     * Method uses Thumbnailator library to decrease size of an image and to make it's thumbnail.
     */
    private void uploadImage() {
        try (InputStream in1 = imageFile.getInputStream();
             InputStream in2 = imageFile.getInputStream();
             FileOutputStream out1 = new FileOutputStream(Util.getImgFolderPath() + "img" + newItem.getId());
             FileOutputStream out2 = new FileOutputStream(Util.getImgFolderPath() + "thumb" + newItem.getId())) {

            //Image from input stream should be resized to fit given maximum dimensions
            //This will also radically decrease size of any camera photos
            Thumbnails.of(in1)
                    .size(1000, 1000)
                    .outputFormat("jpeg")
                    .toOutputStream(out1);
            IOUtils.copy(in1, out1);

            //Create thumbnail
            Thumbnails.of(in2)
                    .size(350, 350)
                    .outputFormat("jpeg")
                    .toOutputStream(out2);
            IOUtils.copy(in2, out2);

            itemsServices.updateItem(newItem);
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    public void createItem() {
        newItem.setDateTime(LocalDateTime.now());
        itemsServices.createItem(newItem);
        if(imageFile != null) {
            uploadImage();
        }
        FacesContext.getCurrentInstance().getViewRoot().getViewMap().clear();
    }
}
