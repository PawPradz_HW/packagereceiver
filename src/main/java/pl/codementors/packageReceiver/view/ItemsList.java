package pl.codementors.packageReceiver.view;

import pl.codementors.packageReceiver.model.Item;
import pl.codementors.packageReceiver.services.*;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Named
@ViewScoped
public class ItemsList implements Serializable {

    @EJB
    private ItemsServices itemsServices;
    @EJB
    private CategoriesServices categoriesServices;
    @EJB
    private DepartmentsServices departmentsServices;
    @EJB
    private WarehousesServices warehousesServices;
    @EJB
    private TagsServices tagsServices;

    private List<Item> items;
    private List<Item> selectedItems;
    private Item selectedItem;

    private List<Item> filteredItems;

    private List<String> tagsAvailable;

    public List<Item> getItems() {
        if(items == null) {
            items = itemsServices.getItems();
        }
        return items;
    }

    public Item getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Item selectedItem) {
        this.selectedItem = selectedItem;
    }

    public List<Item> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(List<Item> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public List<Item> getFilteredItems() {
        return filteredItems;
    }

    public void setFilteredItems(List<Item> filteredItems) {
        this.filteredItems = filteredItems;
    }

    //List of available categories NAMES (strings)
    public List<String> getCategories() {
        List<String> categories = new ArrayList<>();
        categoriesServices.getCategories().forEach(cat -> categories.add(cat.getName()));
        return categories;
    }

    //List of available departments NAMES (strings)
    public List<String> getDepartments() {
        List<String> departments = new ArrayList<>();
        departmentsServices.getDepartments().forEach(dep -> departments.add(dep.getName()));
        return departments;
    }

    //List of available warehouses NAMES (strings)
    public List<String> getWarehouses() {
        List<String> warehouses = new ArrayList<>();
        warehousesServices.getWarehouses().forEach(dep -> warehouses.add(dep.getName()));
        return warehouses;
    }

    //List of available tag NAMES (strings)
    public List<String> getTagsAvailable() {
        if (tagsAvailable == null) {
            tagsAvailable = new ArrayList<>();
            tagsServices.getTags().forEach(tag -> tagsAvailable.add(tag.getName()));
        }
        return tagsAvailable;
    }

    public void deleteSelectedItems() {
        selectedItems.forEach(item -> {
            System.out.println("Deleting: " + item.getId());
            itemsServices.deleteItem(item);
        });
        selectedItems = null;
        items = null;   //force list reloading
    }

    public void editSelectedItem() {
        itemsServices.updateItem(selectedItem);
    }

    /**
     * Filter items viewed in DataTable by selected tags.
     * Object is displayed in data table if function returns true.
     *
     * @param value Value of a cell in filtered table row: string with all item tags split by spaces.
     * @param filter String array of selected tags.
     * @param locale Language setting.
     * @return Boolean: true if item in filtered row has at least one of selected tags, false otherwise.
     */
    public boolean filterByTags(String value, String[] filter, Locale locale) {
        if(filter.length == 0) { //checking filter[] length, because after selecting some tags and later deselecting all of them filter[] will be empty, so this function shouldn't filter out anything anymore
            return true;
        }
        else {
            ArrayList<String> itemTags = new ArrayList<>(Arrays.asList(value.split(" "))); //tags of an item are split by spaces in a single String, so they need to be separated for checking

            boolean itemTagsContainsAtLeastOneSelectedTag = false;

            for (String tag : filter) {
                if (itemTags.contains(tag)) {
                    itemTagsContainsAtLeastOneSelectedTag = true;
                    break;
                }
            }
            return itemTagsContainsAtLeastOneSelectedTag;
        }
    }
}
