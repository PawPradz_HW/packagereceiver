package pl.codementors.packageReceiver.view;

import pl.codementors.packageReceiver.model.Warehouse;
import pl.codementors.packageReceiver.services.WarehousesServices;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class WarehousesList implements Serializable {

    @EJB
    private WarehousesServices warehousesServices;

    private List<Warehouse> warehouses;
    private Warehouse selectedWarehouse;
    private Warehouse newWarehouse;

    public Warehouse getNewWarehouse() {
        if(newWarehouse==null){
            newWarehouse = new Warehouse();
        }
        return newWarehouse;
    }

    public void setNewWarehouse(Warehouse newWarehouse) {
        this.newWarehouse = newWarehouse;
    }

    public List<Warehouse> getWarehouses() {
        if(warehouses == null) {
            warehouses = warehousesServices.getWarehouses();
        }
        return warehouses;
    }

    public void setWarehouses(List<Warehouse> warehouses) {
        this.warehouses = warehouses;
    }

    public Warehouse getSelectedWarehouse() {
        return selectedWarehouse;
    }

    public void setSelectedWarehouse(Warehouse selectedWarehouse) {
        this.selectedWarehouse = selectedWarehouse;
    }

    public void deleteWarehouse() {
        FacesContext context = FacesContext.getCurrentInstance();

        if(warehousesServices.isWarehouseNotUsed(selectedWarehouse)) {
            warehousesServices.deleteWarehouse(selectedWarehouse);
            context.addMessage(null, new FacesMessage("Success!", "Warehouse deleted"));
        } else {
            context.addMessage(null, new FacesMessage("Couldn't delete warehouse!", "Warehouse is in use"));
        }
        warehouses = null;
    }

    public void createWarehouse() {
        warehousesServices.createWarehouse(newWarehouse);
        newWarehouse = null;
        warehouses = null;
    }

    public void editSelectedWarehouse() {
        warehousesServices.updateWarehouse(selectedWarehouse);
    }

}
