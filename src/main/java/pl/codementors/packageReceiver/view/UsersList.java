package pl.codementors.packageReceiver.view;

import org.apache.commons.codec.digest.DigestUtils;

import pl.codementors.packageReceiver.services.UsersServices;
import pl.codementors.packageReceiver.model.User;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class UsersList implements Serializable {

    @EJB
    private UsersServices usersServices;

    private List<User> users;
    private User selectedUser;
    private User newUser;

    private List<SelectItem> roles;

    public List<User> getUsers() {
        if(users == null) {
            users = usersServices.getUsers();
        }
        return users;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public User getNewUser() {
        if(newUser == null) {
            newUser = new User();
        }
        return newUser;
    }

    public void setNewUser(User newUser) {
        this.newUser = newUser;
    }

    public List<SelectItem> getRoles() {
        if (roles == null) {
            roles = new ArrayList<SelectItem>();
            for(int i = 0; i < User.Role.values().length; i++) {
                roles.add(new SelectItem( User.Role.values()[i], User.Role.values()[i].toString().toUpperCase() ));
            }
        }
        return roles;
    }

    public void deleteSelectedUser() {
        FacesContext context = FacesContext.getCurrentInstance();

        if(!usersServices.isUserTheLastAdminLeft(selectedUser)) { //FIXME: selectedUser has always role=null while other fields are ok - ?? ENUM issue ??
            usersServices.deleteUser(selectedUser);
            context.addMessage(null, new FacesMessage("Success!", "User deleted"));
        } else {
            context.addMessage(null, new FacesMessage("Couldn't delete user!", "User is the last admin in database"));
        }
        users = null;
    }

    public void editSelectedUser() {
        usersServices.updateUser(selectedUser);
    }

    /**
     * This method uses usersServices.class instance to add new user to database
     * Before calling .createUser method from usersServices, newUser's password is hashed
     */
    public void createUser() {
        newUser.setPassword(DigestUtils.sha256Hex(newUser.getPassword()));
        usersServices.createUser(newUser);
        newUser = null;
        users = null;
    }


}
