package pl.codementors.packageReceiver.view;


import pl.codementors.packageReceiver.model.Category;
import pl.codementors.packageReceiver.services.CategoriesServices;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class CategoriesList implements Serializable {

    @EJB
    private CategoriesServices categoriesServices;

    private List<Category> categories;
    private Category selectedCategory;
    private Category newCategory;

    public Category getNewCategory() {
        if(newCategory == null) {
            newCategory = new Category();
        }
        return newCategory;
    }

    public void setNewCategory(Category newCategory) {
        this.newCategory = newCategory;
    }

    public List<Category> getCategories() {
        if(categories == null) {
            categories = categoriesServices.getCategories();
        }
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Category selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public void deleteCategory() {
        FacesContext context = FacesContext.getCurrentInstance();

        if(categoriesServices.isCategoryNotUsed(selectedCategory)) {
            categoriesServices.deleteCategory(selectedCategory);
            context.addMessage(null, new FacesMessage("Success!", "Category deleted"));
        } else {
            context.addMessage(null, new FacesMessage("Couldn't delete category!", "Category is in use"));
        }
        categories = null;
    }

    public void createCategory() {
        categoriesServices.createCategory(newCategory);
        newCategory = null;
        categories = null;
    }

    public void editSelectedCategory() {
        categoriesServices.updateCategory(selectedCategory);
    }
}
