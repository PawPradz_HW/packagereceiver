package pl.codementors.packageReceiver.view;

import pl.codementors.packageReceiver.model.Department;
import pl.codementors.packageReceiver.services.DepartmentsServices;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class DepartmentsList implements Serializable {

    @EJB
    private DepartmentsServices departmentsServices;

    private List<Department> departments;
    private Department selectedDepartment;
    private Department newDepartment;

    public List<Department> getDepartments() {
        if(departments == null) {
            departments = departmentsServices.getDepartments();
        }
        return departments;
    }

    public Department getNewDepartment() {
        if(newDepartment == null) {
            newDepartment = new Department();
        }
        return newDepartment;
    }

    public void setNewDepartment(Department newDepartment) {
        this.newDepartment = newDepartment;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public Department getSelectedDepartment() {
        return selectedDepartment;
    }

    public void setSelectedDepartment(Department selectedDepartment) {
        this.selectedDepartment = selectedDepartment;
    }

    public void deleteDepartment() {
        FacesContext context = FacesContext.getCurrentInstance();

        if(departmentsServices.isDepartmentNotUsed(selectedDepartment)) {
            departmentsServices.deleteDepartment(selectedDepartment);
            context.addMessage(null, new FacesMessage("Success!", "Department deleted"));
        } else {
            context.addMessage(null, new FacesMessage("Couldn't delete department!", "Department is in use"));
        }
        departments = null;
    }

    public void createDepartment() {
        departmentsServices.createDepartment(newDepartment);
        newDepartment = null;
        departments = null;
    }

    public void editSelectedDepartment() {
        departmentsServices.updateDepartment(selectedDepartment);
    }

}
