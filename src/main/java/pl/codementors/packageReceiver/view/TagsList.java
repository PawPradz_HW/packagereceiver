package pl.codementors.packageReceiver.view;


import pl.codementors.packageReceiver.model.Tag;
import pl.codementors.packageReceiver.services.TagsServices;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class TagsList implements Serializable {

    @EJB
    private TagsServices tagsServices;

    private List<Tag> tags;
    private Tag selectedTag;
    private Tag newTag;

    public Tag getNewTag() {
        if(newTag == null){
            newTag = new Tag();
        }
        return newTag;
    }

    public void setNewTag(Tag newTag) {
        this.newTag = newTag;
    }

    public List<Tag> getTags() {
        if(tags == null) {
            tags = tagsServices.getTags();
        }
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Tag getSelectedTag() {
        return selectedTag;
    }

    public void setSelectedTag(Tag selectedTag) {
        this.selectedTag = selectedTag;
    }

    public void deleteTag() {
        FacesContext context = FacesContext.getCurrentInstance();

        if(tagsServices.isTagNotUsed(selectedTag)) {
            tagsServices.deleteTag(selectedTag);
            context.addMessage(null, new FacesMessage("Success!", "Tag deleted"));
        } else {
            context.addMessage(null, new FacesMessage("Couldn't delete tag!", "Tag is in use"));
        }
        tags = null;
    }

    public void createTag() {
        tagsServices.createTag(newTag);
        newTag = null;
        tags = null;
    }

    public void editSelectedTag() {
        tagsServices.updateTag(selectedTag);
    }
}
