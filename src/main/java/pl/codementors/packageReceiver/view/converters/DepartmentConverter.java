package pl.codementors.packageReceiver.view.converters;

import pl.codementors.packageReceiver.model.Department;
import pl.codementors.packageReceiver.services.DepartmentsServices;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = Department.class)
public class DepartmentConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        DepartmentsServices service = CDI.current().select(DepartmentsServices.class).get();
        if (value == null || value.equals("null")) {//No value or nothing selected in html.
            return null;
        }
        return service.getDepartment(Integer.parseInt(value));//Parse id in string to integer.
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value == null) {
            return "null";//Return anything to say that author is null.
        }
        return ((Department) value).getId() + "";//Return unique name which will be used in html form.
    }
}
