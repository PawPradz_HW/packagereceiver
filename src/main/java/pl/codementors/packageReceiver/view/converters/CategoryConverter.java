package pl.codementors.packageReceiver.view.converters;

import pl.codementors.packageReceiver.model.Category;
import pl.codementors.packageReceiver.services.CategoriesServices;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = Category.class)
public class CategoryConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        CategoriesServices service = CDI.current().select(CategoriesServices.class).get();
        if (value == null || value.equals("null")) {//No value or nothing selected in html.
            return null;
        }
        return service.getCategory(Integer.parseInt(value));//Parse id in string to integer.
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value == null) {
            return "null";//Return anything to say that author is null.
        }
        return ((Category) value).getId() + "";//Return unique name which will be used in html form.
    }
}
