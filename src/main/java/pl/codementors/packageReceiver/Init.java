package pl.codementors.packageReceiver;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import org.apache.commons.codec.digest.DigestUtils;
import pl.codementors.packageReceiver.model.*;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for loading sample data to the database
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@Singleton
@Startup
public class Init {

    /**
     * EntityManager to communicate with database
     */
    @PersistenceContext
    private EntityManager em;

    /**
     * Method populating database on startup
     */
    @PostConstruct
    public void init() {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        query.from(User.class);
        if (em.createQuery(query).getResultList().isEmpty()) {
            User defaultAdmin = new User();
            defaultAdmin.setLogin("admin");
            defaultAdmin.setPassword(DigestUtils.sha256Hex("admin"));
            defaultAdmin.setName("defaultName");
            defaultAdmin.setSurname("defaultSurname");
            defaultAdmin.setRole(User.Role.ADMIN);
            em.persist(defaultAdmin);

            User defaultUser = new User();
            defaultUser.setLogin("userLogin");
            defaultUser.setPassword(DigestUtils.sha256Hex("user"));
            defaultUser.setName("userName");
            defaultUser.setSurname("userSurname");
            defaultUser.setRole(User.Role.USER);
            em.persist(defaultUser);
        }

        Lorem lorem = LoremIpsum.getInstance();

        List<Tag> tagList = new ArrayList<>();
        List<Category> categoryList = new ArrayList<>();
        List<Department> departmentList = new ArrayList<>();
        List<Warehouse> warehouseList = new ArrayList<>();

        String[] tagNames = {"RTG", "ASC", "STS", "RS", "IMV", "RMG", "AGV", "Safety", "Comm", "Opp", lorem.getWords(1).toUpperCase()};
        for (String t : tagNames) {
            Tag tag = new Tag();
            tag.setName(t);
            tagList.add(tag);
            em.persist(tag);
        }

        String[] categoryNames = {"Spare parts", "Hand tools", "Power tools", "Devices", "Safe wear", "Motors", "Gearboxes", "Gearbox oil", "Other"};
        for (String c : categoryNames) {
            Category category = new Category();
            category.setName(c);
            categoryList.add(category);
            em.persist(category);
        }

        String[] departmentNames = {"Technical", "Operation", "IT", "Safety", "HR"};
        for (String d : departmentNames) {
            Department department = new Department();
            department.setName(d);
            departmentList.add(department);
            em.persist(department);
        }

        String[] warehousesNames = {"Main", "Secondary", "Shore T1", "Shore T2", "Hall H1", "Hall H2", "External"};
        for (String w : warehousesNames) {
            Warehouse warehouse = new Warehouse();
            warehouse.setName(w);
            warehouseList.add(warehouse);
            em.persist(warehouse);
        }

        for (int i = 0; i < 120; i++) {
            Item item = new Item();
            item.setName(lorem.getWords(1,5));
            item.setCategory(categoryList.get(randBetween(0,8)));
            item.setDepartment(departmentList.get(randBetween(0,4)));
            item.setWarehouse(warehouseList.get(randBetween(0,6)));

            for (int j : getDifferentRandsBetween(randBetween(1,5),0,10)) { //random amount (in given range) of different numbers in given range
                item.getTags().add(tagList.get(j));
            }

            LocalDateTime dateTime = LocalDateTime.of(
                    randBetween(1999, 2018),
                    randBetween(1,12),
                    randBetween(1,27),
                    randBetween(0,23),
                    randBetween(0,59),
                    randBetween(0,59));
            item.setDateTime(dateTime);

            em.persist(item);
        }

        Category unassignedCategory = new Category();
        unassignedCategory.setName("UNASSIGNED CATEGORY");
        em.persist(unassignedCategory);

        Department unassignedDepartment = new Department();
        unassignedDepartment.setName("UNASSIGNED DEPARTMENT");
        em.persist(unassignedDepartment);

        Warehouse unassignedWarehouse = new Warehouse();
        unassignedWarehouse.setName("UNASSIGNED WAREHOUSE");
        em.persist(unassignedWarehouse);

        Tag unassignedTag1 = new Tag();
        unassignedTag1.setName("UN1");
        em.persist(unassignedTag1);
        Tag unassignedTag2 = new Tag();
        unassignedTag2.setName("UN2");
        em.persist(unassignedTag2);
    }

    private int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }

    private int[] getDifferentRandsBetween(int quantity, int start, int end) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        int[] differentRands = new int[quantity];
        for (int i = start; i < end+1; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        for (int i=0; i<quantity; i++) {
            differentRands[i]=list.get(i);
        }
        return differentRands;
    }
}