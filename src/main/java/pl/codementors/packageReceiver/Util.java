package pl.codementors.packageReceiver;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Named;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class for handling images
 * @author Paweł Prądzyński
 * @author Damian Modzelewski
 */

@Named
@ApplicationScoped
public class Util {

    private static final Logger log = Logger.getLogger(Util.class.getName());

    /**
     * This method takes as an input type of an image to be displayed and reads requested image id from FacesContext
     * RequestParameterMap. Then it returns image form specified folder if it exists. If not, default image is returned.
     *
     * @param type type of an image - img or thumb
     * @return image as a streamed content (default image if image with given id doesn't exist)
     */
    private StreamedContent getImageFile(String type) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        } else {
            // So, browser requests the image. Return a real StreamedContent with the image.
            String idAsParam = context.getExternalContext().getRequestParameterMap().get("id");
            String imageFilePath = getImgFolderPath() + type + idAsParam;
            File imageFile = new File(imageFilePath);
            DefaultStreamedContent dsc;
            if(imageFile.exists()) {
                try {
                    dsc = new DefaultStreamedContent(new FileInputStream(imageFilePath));
                } catch(IOException ex) {
                    log.log(Level.WARNING, ex.getMessage() + "\n\t\tImage file couldn't be opened. Item id: " + idAsParam);
                    dsc = null;
                }
            } else {
                String defaultImageFilePath = getImgFolderPath() + type + 0;
                try {
                    dsc = new DefaultStreamedContent(new FileInputStream(defaultImageFilePath));
                } catch(IOException ex) {
                    log.log(Level.WARNING, ex.getMessage() + "\n\t\tDefault " + type + " couldn't be opened. Item id: " + idAsParam);
                    dsc = null;
                }
            }
            return dsc;
        }
    }

    /**
     * Method used to parameterize getImageFile() method so that it would return full size image.
     *
     * @return full size image as a streamed content
     */
    public StreamedContent getImage() {
        return getImageFile("img");
    }

    /**
     * Method used to parameterize getImageFile() method so that it would return thumbnail.
     *
     * @return thumbnail as a streamed content
     */
    public StreamedContent getThumbnail() {
        return getImageFile("thumb");
    }

    /**
     * This method reads imgFolderPath parameter from config.properties file
     * Parameter imgFolderPath describes where images are stored on server's drive
     *
     * @return imgFolderPath parameter value if it's possible to read, null otherwise
     */
    public static String getImgFolderPath() {
        Properties prop = new Properties();
        try(InputStream stream = Util.class.getResourceAsStream("/config.properties"))  {
            prop.load(stream);
            return prop.getProperty("imgFolderPath");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Method for checking if image with given id exists on server's drive.
     * Method is use for disabling links (items.xhtml) and disabling rendering of icons in dataTable (items_admin.xhtml)
     *
     * @param id index of an image to be checked
     * @return true if image exists, false otherwise
     */
    public boolean imageExists(int id) {
        File imageFile = new File(getImgFolderPath() + "img" + id);
        return imageFile.exists();
    }
}
