package pl.codementors.packageReceiver.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="items")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private LocalDateTime dateTime;

    @ManyToOne
    @JoinColumn(name = "category", referencedColumnName = "id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "department", referencedColumnName = "id")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "warehouse", referencedColumnName = "id")
    private Warehouse warehouse;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "items_tags", joinColumns = @JoinColumn(name = "item", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "tag", referencedColumnName = "id"))
    private List<Tag> tags = new ArrayList<>();

    @Transient
    private String tagsAsString;

    public Item() {
    }

    public Item(String name, LocalDateTime dateTime, Category category, Department department, Warehouse warehouse, List<Tag> tags) {
        this.name = name;
        this.dateTime = dateTime;
        this.category = category;
        this.department = department;
        this.warehouse = warehouse;
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTime.format(formatter);
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public List<Tag> getTags() {
        if(tags == null) {
            tags = new ArrayList<>();
        }
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                Objects.equals(name, item.name) &&
                Objects.equals(dateTime, item.dateTime) &&
                Objects.equals(category, item.category) &&
                Objects.equals(department, item.department) &&
                Objects.equals(warehouse, item.warehouse) &&
                Objects.equals(tags, item.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, dateTime, category, department, warehouse, tags);
    }

    public String getTagsAsString() {
        if(tagsAsString == null) {
            StringBuilder sb = new StringBuilder();
            getTags().forEach(tag -> {
                sb.append(tag.getName());
                sb.append(" ");
            });
            return tagsAsString = sb.toString();
        }
        else return tagsAsString;
    }
}
