Package Receiver Application - final project @ Codementors Junior Java Developer course

authors:
   Pawel Pradzynski
   Damian Modzelewski


---------------------------------------------------------------------------------------
---                           README - TABLE OF CONTENTS                            ---
---------------------------------------------------------------------------------------

   1. PROJECT DESCRIPTION & PRINCIPLES

   2. TECHNOLOGIES USED

   3. HOW TO RUN THE APPLICATION

   4. TO BE DONE

---------------------------------------------------------------------------------------
---     1. PROJECT DESCRIPTION & PRINCIPLES                                         ---
---------------------------------------------------------------------------------------

Main purpose of this project was to create an application that would allow registering
items delivered to a particular company's warehouse. 

Application is reached by browser. On the main page there is a list of all the
previously added items. Items list is sortable and searchable by item's name, date of
adding, assigned category/department/warehouse/tags or a combination of these.
If an item has a photo assigned, it can be displayed from items list level. Mentioned
functionalities are available with no restrictions.

Apart from that, application provides 2 different access levels: user and admin.

After logging in, user gets an option of adding new items to the database. When adding
new item, user can assign to it one of available categories/departments/warehouses and
chose one or many predefined tags. It's also possible to upload an image.

Administrator has a possibility to edit, delete or predefine new items' categories,
warehouses, departments and tags. He has also rights to delete items from database and
register new users or delete/edit existing ones.

---------------------------------------------------------------------------------------
---     2. TECHNOLOGIES USED                                                        ---
---------------------------------------------------------------------------------------

Application is based on JavaServer Faces with PrimeFaces and uses MySQL database.

---------------------------------------------------------------------------------------
---     3. HOW TO RUN THE APPLICATION                                               ---
---------------------------------------------------------------------------------------

Apart from cloning app's repository & building project, it is required to create
a MySQL database and configure application server (JBOSS / WildFly) to use it.

It is also necessary to write a right jta-data-source name in persistencce.xml file
and a valid images folder path in config.properties file before building the app.

---------------------------------------------------------------------------------------
---     4. TO BE DONE                                                               ---
---------------------------------------------------------------------------------------

 - protection against deleting last admin user from database doesn't work

 - update README so that it's more exhaustive 